nakeDeb
=======

[nakeDeb homepage](https://nakedeb.arpinux.org)

* dépendances pour les paquets debian : equivs
* dépendances pour live-build : live-build
* dépendance pour construire le fichier torrent : transmission

construire une ISO :
--------------------

* éditer le script 'nakedbuild' pour ajuster les variables
* éditer le contenu de 'auto/' et 'config/' selon vos besoins
* lancer **en mode administrateur** :
  * `./nakedbuild 32`           > nakedeb i386
  * `./nakedbuild 64`           > nakedeb amd64
  * `./nakedbuild clean`        > nettoyage du dossier de construction et du cache

assurez-vous de lancer `nakedbuild clean` avant de construire une autre architecture

------

option : apt-cacher-ng

* installer : `apt install apt-cacher-ng`
* configurer : `export http_proxy=http://localhost:3142/`
* activer : `systemctl start apt-cacher-ng`
* utiliser : déjà intégré dans le script de build

------

option : signer le fichier sha256 pour distribuer l'ISO

`gpg --detach-sig --sign nakedeb-${VERSION}.sha256`

en remplaçant ${VERSION} par la version de votre nakedeb.

------

aide, contact, retours :

* forum : https://nakedeb.arpinux.org/support/
* IRC : #nakedeb on oftc.net
* contact : nakedeb@arpinux.org
