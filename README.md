nakeDeb | a naked Debian *but safe for work* by arpinux
=======================================================

[nakeDeb homepage](https://nakedeb.arpinux.org)

ce qu'est nakeDeb :
-------------------
**nakeDeb** est une *dérivée* française (mais qui sait parler anglais) basée 
sur Debian prévue pour délivrer une distribution debian ultra-minimale vous 
permettant d'installer rapidement une debian fonctionnelle ou de l'utiliser 
en live léger de secours.

**nakeDeb** tourne sur Debian stable et utilise fluxbox comme gestionnaire de 
fenêtres par défaut. i3wm l'accompagne avec une session en tiling pilotable au 
clavier. pas d'environnement de bureau complet, noDE!

**nakeDeb** n'est pas particulièrement destinée aux novices mais peut être prise 
en main par une personne curieuse ;)

**nakeDeb** est construite de façon modulaire : tous les ajouts sont réalisés 
sous forme de paquets debian "classique" qu'il suffit de désinstaller pour 
obtenir une Debian "pure".

**nakedeb** est distribuée en version amd64 et i386.

ce que *n'est pas* nakeDeb :
--------------------------
**nakeDeb** n'est pas une distribution communautaire.. c'est surtout un projet 
minimal ludique. pour un usage "sérieux", préférez une Debian.

charte nakeDeb :
----------------
* **nakeDeb** n'utilise pas les dépôts non-free par défaut... 
sauf pour les 'non-free-firmware' comme Debian.
* **nakeDeb** est basée sur Debian et respecte ses règles et licence : distribution 
des sources sous licence libre et utilisation de dépôts officiels sécurisés.
* **nakeDeb** tente de se rapprocher le plus possible de la philosophie KISS : un 
outil pour chaque tâche. ceci-dit, quelques applications graphiques se sont 
glissées dans l'ISO...
* nakedeb est une distribution française avec tout plein de franglais dedans car 
soyons honnêtes, si vous désirez vous amuser avec une Debian minimale, vous 
devrez passer par un peu d'english de temps en temps ;) .
* ... liste non-exhaustive soumise à modifications ... ni dieu ni maître.

sources :
---------
les sources de la version stable sont [disponibles sur git](https://framagit.org/3hg/nakedeb/-/releases)

howto :
-------
pour construire votre nakeDeb, direction le [HowTo](/HOWTO.md)

licence :
---------
nakeDeb est livrée sous licence libre [WTFPL](https://nakedeb.arpinux.org/LICENCE)
```
+------------------------------+
|               ~~:~~          |
|                 ~:+=:~       |
|         ~~~~~      ~+o=:     |
|     ~+========+:     :=o=:   |
|   :==oooo=o===oo=:    ~=o=:  |
|  :====o==o======oo+    :o==~ |
| ~====o=o==o=oo=o===:   ~===+ |
| :====ooooooo=ooo=o=+    ==== |
| ~==+:::::+=========:   ~=oo+ |
|  ~  ~~~~~  :======+    :o==~ |
|   ~+=ooo==~  ====:    :=o=:  |
|   =ooo==o==~ ~+:     :o==~   |
|   +==ooo===~       ~===:     |
|    +=====+~     ~:=+:~       |
|      ~~~~     ~~~~           |
+------------------------------+
  nakeDeb -but safe for work-
```
