# nakeDeb

a naked Debian -but safe for work- by arpinux
WTFPL © 2020-2025 https://nakedeb.arpinux.org

2025-03-16 - nakedeb-1.5.10
---------------------------
* mise à jour Debian-12.10
* correction de l'adresse des thumbnails sur dotcleaner
* ajout du switch-user dans le nakedquit (thx Croutons)
* mise à jour de la documentation (thx zbig)
* refonte du site internet
  * documentation et site internet adaptés clair/sombre
  * ouverture du nakedBlog https://nakedeb.arpinux.org/blog/
* suppression de l'option '-u' du `nakedupdate`
* mise à jour de la config dunst
* mise à jour de la config lf
* mise à jour de la config fluxbox
* btop remplace htop
* improve build script

2025-01-11 - nakedeb-1.5.9
--------------------------
* mise à jour Debian-12.9
* mise à jour de la documentation
* ajout du raccourci Ctrl+Alt+r pour relancer fluxbox (thx @Croutons)
* correction typo ~/.Xresources (thx @Croutons)
* ajout du paragraphe i3lock dans le wiki (thx @Aiga_bolhida)
* ajout et activation de apparmor
* suppression de low-memory-monitor
* suppression de gparted (doublon gnome-disks-utility)
* nettoyage des sources

2024-11-10 - nakedeb-1.5.8
--------------------------
* mise à jour Debian-12.8
* remplacement de ncdu par gdu (thx @yoludake)
* amélioration de la config lf
  * 'escape' pour `clear`
  * 'a' pour `touch` & 'A' pour `mkdir`
  * meilleur rendu 'highlight'
* ajout de l'agent d'authentification pour i3
* correction des window-rules pour (F|f)irefox sur i3 et sway
* correction du bug des paquets ajoutés puis supprimés

2024-09-01 - nakedeb-1.5.7
--------------------------
* mise à jour Debian-12.7
* mise à jour de la documentation
* correction de la configuration i3-english (thx @olivier)
* passage à sha256sum pour la vérification des ISOs (thx @wardidi)
* ajout du lock avant hibernate (thx @3r1c)
* nettoyage des sources

2024-06-30 - nakedeb-1.5.6
--------------------------
* mise à jour Debian-12.6
* mise à jour de la documentation
* mise à jour de la configuration i3
* mise à jour du mode resize sur i3
* mise à jour de la config fluxbox
* correction des lanceurs `urxvtc -tr` qui font planter X
* ajout de l'autolock by xautolock
* ajout de l'utilitaire trash-cli
* suppression de nakedroot
* suppression de nakedfm
* meilleure déclaration des Urxvt.font dans ~/.Xresources
* remplacement de ranger par son successeur, lf (thx @zbig)
* remplacement de pulseaudio par pipewire
* remplacement de sxiv par nsxiv (thx @zbig)
* mise à jour de la configuration i3status
* mise à jour de la configuration polybar
  * ajout du script screntemp pour i3/polybar
* suppression des appels sct/sctd
* mise à jour de fluxbox-automenu (thx @croutons)
  * tri des applications de 'Applist'
  * auto-exclude les applications 'NoDisplay'
* refonte de nakedquit :
  * lock avant veille
  * accepte les arguments
  * locker unifié
  * suppression de i3lock-fancy
* simplification de la déclaration des PATH (thx @zbig)
* mise à jour de la config neofetch
* mise à jour de la config zathura
* refonte gtk-theme&look
  * ajout de 4 walls par tyzef (thx)
  * ajout du thème nord pour geany
  * ajout du thème Nordic GTK
  * ajout du thème d'icônes Zafiro Nord Dark
  * ajout du thème de curseur Nordzy Dark
  * suppression du GTK-Arc-theme
* mise à jour nakedsway (thx @zbig) + doc dans wiki
* suppression de /dev/sda comme disque principal lors de l'installation :
  le choix est désormais laissé à l'utilisateur.

2024-02-11 - nakedeb-1.5.5
--------------------------
* mise à jour Debian-12.5
* mise à jour de la configuration de GRUB
* correction du bashrc : prise en compte des Path system (thx Darks)
* correction de la configuration de dunst
* correction du message d'accueil (thx Steve S.)
* ajout de systemd-timesyncd - thx @Croutons
* ajout de l'option -u(upgrade) au nakedupdate
  pour afficher la date de la dernière mise à jour (thx Tawal)
* suppression de xserver-xorg-input-synaptics (inutile)
* suppression de loadlin du live
* suppression des paquets live* après installation
* préparation pour sway/wayland

2023-12-11 - nakedeb-1.5.4
--------------------------
* mise  jour Debian 12.4

2023-12-10 - nakedeb-1.5.3
--------------------------
**Version non publiée**

* mise à jour Debian 12.3
* mise à jour de la documentation
* mise à jour de la configuration zathura (thx @Croutons)
* mise à jour de eyecandy (better picom conf)
* personnalisation de l'entrée GRUB

2023-10-08 - nakedeb-1.5.2
--------------------------
* mise à jour Debian 12.2
* suppression de picom & eyecandy
* configuration de neofetch
* ajout de unclutter-xfixes
* dotcleaner :
  * ajout de l'input-history de zathura (thx Steve S.)
  * ajout des logs weechat
  * ajout des historiques aspell & ispell (thx Steve S.)
  * correction du nettoyage de profanity
  * correction des commandes truncate
  * précisions sur les fichiers affectés
* suppression de l'alias killzombies du bashrc (thx Steve S.)
* typo dans la doc (thx Steve S.)
* Ctrl+Shift+c/v pour le copier/coller dans urxvt (thx Steve S.)
* amélioration de la conf mpv (thx Steve S.)
* ajout d'un temps d'attente avant le nakedupdate (thx Croutons)
* correction de la conf i3/i3status (thx Steve S.)
* ajout de l'utilisation de nethogs sans droits admin
* amélioration de la statusbar de sxiv (thx Steve S.)
* ajout de quelques walls

2023-07-23 - nakedeb-1.5.1
--------------------------
* mise à jour Debian 12.1
* suppression des firmwares inutiles (thx raleur@df)
* mise à jour de la documentation i3 (traduction)
* update du login wall
* ajout de smart-nofitier
* ajout du nakedaudit dans les nakedtools
* verbose live/install boot

2023-06-25 - nakedeb-1.5.0
--------------------------
* mise à jour Debian 12.0 stable
* mise à jour des paquets naked*
  * suppression des manuels nakedeb,nakedcli et nakedkbd
  * mise à jour de la configuration zathura (thx Steve S.)
  * mise à jour de la configuration i3status (thx Steve S.)
  * mise à jour de la documentation intégrée
* ajout de low-memory-monitor

2023-05-18 - nakedeb-1.5.0-RC
-----------------------------
- basée sur Debian 12
- suppression des versions free/non-free : les non-free-firmware sont intégrés par défaut (as Debian)
- mise à jour des paquets naked*
- suppression de nakedconky : retour à un seul conky pour fluxbox
- nettoyage des nakedtools
- remlacement de sct par redshift
- suppression de memtest

2022-12-25 - nakedeb-1.4.6
--------------------------
* mise à jour Debian 11.6
* correction du terminal dans geany
* configuration graphique des notifications
* amélioration de la configuration picom
* correction du lancement de sctd
* suppression du forum de support
* mise à jour de nakedhelp : simplification en lanceur de wiki
* mise à jour de la documentation : ajout de la section notifications
* prise en charge du dossier local d'applications dans fluxbox-automenu

2022-07-10 - nakedeb-1.4.4
--------------------------
* mise à jour Debian 11.4
* mise à jour de la documentation :
  * ajout de la section reset pour chaque session
  * aout de l'import de la clé publique GPG
* mise à jour de la configuration fluxbox :
  * activation de fluxbox-remote
* mise à jour du message d'accueil (de w3m à rofi)
* ajout de l'hibernation dans le nakedquit (thx @croutons)
* correction du script nakedspace (thx @croutons)

2022-03-27 - nakedeb-1.4.3
--------------------------
* mise à jour Debian 11.3
* mise à jour de la documentation
* mise à jour des manuels
* remplacement de VeraCrypt par zuluCrypt
* migration du forum vers NoNonsenseForum
* harmonisation des raccourcis clavier
* petits fix du script de build

2021-12-20 - nakedeb-1.4.2
--------------------------
* mise à jour Debian 11.2
* mise à jour des outils nakeDeb
* mise à jour de VeraCrypt
* ajout du nakedupdate pour les notifications des mises à jour disponibles
* ajout du fbautostart en début de session i3wm pour les notifications au démarrage
* mise à jour de la base de données mlocate lors de l'installation
* nettoyage des paquets vim

2021-10-10 - nakedeb-1.4.1
--------------------------
* mise à jour Debian 11.1
* mise à jour des outils nakeDeb
* mise à jour de l'aide intégrée
* mise à jour de la config polybar
* dotcleaner : ajout des options sécurisées (thx @Chris)
* nakedspace : ajout de la notification d'espace restreint (thx @Otyugh)
* correction du lancement d'URxvt
* correction des naked'manpages
* ajout de apt-config-auto-update
* ajout de at-spi2-core et dbus-user-session

2021-08-16 - nakedeb-1.4
------------------------
* mise à jour Debian 11.0
* intégration des modules dans un dépôt externe au format debian
  https://nakedeb.arpinux.org/repo
* mise à jour du script de build
* mise à jour de la documentation et du man nakedeb
* correction de la page WebIRC (bad iframe)
* mise à jour de la conf weechat (welcome oftc, exit freenode)
* ajout de cryptsetup pour le chiffrement des volumes internes ou externes
* ajout iotop et nethogs pour la surveillance
* mise à jour de dotcleaner : ajout du .desktop pour une visibilité dans les menus
* remplacement de compton par picom
* suppression de l'instance searx
* correction du mode d'installation expert (merci ospring)
* avertissement de la conf réseau dans la doc (merci Mr.S)
* ajout des principaux firmwares dans la version non-free pour une
  compatibilité accrue : firmware-linux firmware-linux-nonfree
  firmware-misc-nonfree firmware-iwlwifi intel-microcode firmware-realtek
  amd64-microcode firmware-amd-graphics bluez-firmware firmware-atheros
  firmware-bnx2 firmware-bnx2x firmware-ipw2x00 firmware-brcm80211
  firmware-b43-installer firmware-b43legacy-installer
* ajout de l'option 'full' qui complète les nonfree pour obtenir les
  firmwares présents dans la Debian-nonfree + une série d'applications.
* refonte du logo

2021-05-02 - nakedeb-1.3-prx
----------------------------
* mise à jour Debian 10.9
* mise à jour de la documentation
* ajout de memtest
* ajout du mode rescue
* ajout de x11-apps
* ajout de keepassxc (merci atsuhito)
* ajout de "rename" (thx yoludake)
* mise à jour de dotcleaner : ajout de l'historique
* configuration zathura (merci ayb)
* correction de l'heure sur la session live-fr
* correction du bug de déplacement des containers i3
  sur ws11 & ws12 (thx yoludake)
* happy birthday prx :P

2021-02-12 - nakedeb-1.2
------------------------
* ajout d'une version anglophone
* mise à jour Debian 10.8
* mise à jour de la documentation
* mise à jour des modules
* traduction des modules
* traduction de la documentation
* rendre le boot verbeux (thx Hackphil)
* amélioration de l'outil nakedlocate (thx bunsenlabs community)
* ajout d'un thème Fluxbox clair 'nakedNordclear'
* remplacement de katarakt par zathura comme lecteur de PDF (thx yoludake)
* remplacement de screenfetch par neofetch
* ajout d'un fond d'écran par Péhä (@peha@framapiaf.org)
* ajout du conkyrc 'Hackphil'
* résolution du bug du systray dans polybar
* mise en place d'un compte mastodon @nakedeb@chapril

2021-01-17 - nakedeb-1.1
------------------------
* intégration de polybar en dualmode (icon/texte) et de son wiki
* intégration de i3blocks avec les scripts d'Anachron
  https://github.com/Anachron/i3blocks
* ajout du popmenu
* nakeDeb official logo setup & harmonisation des icones naked'apps
* remplacement de cyclope par sxiv
* mise à jour des naked'apps
* mise à jour de la documentation i3wm pour i3status, i3blocks et conky
* mise à jour de la doc nakedeb en man7
* ajout d'un screencast nakeDeb
* suppression du nakedsystray
* fix english keyboard par défaut
* fix doublon conkyrc_fluxbox

2020-12-12 - nakedeb-1.0
------------------------
* export des sources sur git
* mise à jour Debian 10.7
* mise en place d'un espace de support en ligne Q&A
* mise à jour de la documentation nakedWiki
* export de la doc au format manpage (nakedeb, nakedcli & nakedkbd)
* retour du nakedQuit en mode 'icon' pour une fermeture au clavier
* séparation et multiplication des modules
  pour une gestion plus fine (9 modules + 5 paquets)
* correction des modules (lintian check)
* ajout de openssh-client
* ajout de ffmpeg
* ajout de sctd pour la gestion automatique de la température
  d'écran - from OpenBSD, thx prx
* ajout de nakedfm adapté de 'dfm' par prx
* ajout de nakedlocate, un script de recherche basé sur locate et rofi
* ajout de wipe (associé à secure-delete)
* ajout d'une page de documentation keyboard-friendly
* ajout d'un thème rofi nakeDeb.rasi
* ajout du choix du hostname à l'installation (thx yoludake)
* ajout d'une version non-free pour la compatibilité en mode rescue
* refonte du menu fluxbox
* correction et empaquetage du thème d'icônes Clarity
* correction et ajout pour nakedConky
* correction du i3config (thx yoludake)
* suppression du nakedPack
* suppression de localepurge
* suppression du touchpadtap
* suppression de screenshot
* improve build script (thx captnfab, otyugh & valde)

2020-10-17 - nakedeb-0.1-RC
---------------------------
* correction de l'écran de verrouillage
* correction de l'entrée de menu fluxbox lock
* harmonisation des raccourcis
* mise à jour du nakedPacks
* mise à jour des nakedTools
* ajout de bashmount en .deb
* ajout de la police Inconsolata Nerd Font Mono
* ajout de zram-tools
* ajout de localepurge
* ajout de dvtm, dfc & fbi
* ajout de secure-delete (srm)
* ajout de veracrypt
* ajout du $USER/.conky garni
* export de la documentation en .deb
* export de la configuration $USER en .deb
* export de la configuration $ROOT en .deb
* mise à jour de la documentation
* xpdf remplacé par katarakt (thx vv222)
* suppression de volumeicon

2020-10-03 - nakedeb-0.1-beta
-----------------------------
* rédaction d'un README.md et d'un HOWTO.md
* export de cyclope en .deb
* export de fluxbox-automenu en .deb
* export des nakedtools en .deb
* exit i3quit
* traduction du guide de l'utilisateur i3
* peaufinage documentation
* peaufinage du script de build
* ajout de geany
* catfish remplacé par rofi-locate
* peaufinage du thème fluxbox + renommage
* lien vers "more walls"
* ajout de l'espace vidéos
* ajout de quelques walls

2020-09-19 - nakedeb-0.1-alpha
------------------------------
* first build
