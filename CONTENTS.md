nakeDeb sources content
=======================

les sources nakeDeb contiennent de quoi construire 
la distribution livrée sous forme d'ISO  
les paquets additionnels sont issus du dépôt git 
https://framagit.org/3hg/nakedpkgs

[auto](/auto)
-------------
- [auto/build](/auto/build) : live-build build script > chope la version dans 
le ./nakedbuild pour y ajouter les logs de build
- [auto/clean](/auto/clean) : live-build clean script > nettoyer le dossier 
de build après contruction
- [auto/config](/auto/config) : live-build config script > définir certaines 
options de lb config pour toutes les versions

[config](/config)
-----------------
- [config/archives](/config/archives/) : dépôt et clé gpg nakeDeb
- [config/hooks](/config/hooks/) : script lancé lors du build dans le chroot, 
juste avant la compression en squashfs
- [config/includes.binary](config/includes.chroot/) : fichiers de configuration 
pour isolinux et grub, les lanceurs pour le liveCD
- [config/package-lists](config/package-lists/) : les listes des paquets nécessaires

[ressources](/ressources)
-------------------------
- [ressources/img](/ressources/img/) : sources pour le logo et les éléments graphiques.
- [ressources/nakedpreseed](/ressources/nakedpreseed) : le fichier de 
configuration preseed pour nakeDeb (récupéré lors du build)

[nakedbuild](/nakedbuild)
-------------------------
script principal de construction de l'ISO. commenté.

[howto](/HOWTO.md)
------------------
mode d'emploi complet
