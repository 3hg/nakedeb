* refonte méthode d'importation de la clé GPG du dépôt nakeDeb https://www.debian-fr.org/t/apt-ajout-correct-dune-cle-gpg/85278
* ajouter la possibilité de personnaliser le dotcleaner pour qu'il prenne en charge une série de fichiers/dossiers spécifiés dans un fichier de conf du $HOME
